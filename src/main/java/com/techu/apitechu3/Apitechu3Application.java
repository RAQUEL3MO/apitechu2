package com.techu.apitechu3;

import com.techu.apitechu3.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Apitechu3Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(Apitechu3Application.class, args);

		Apitechu3Application.productModels = Apitechu3Application.getTestData();

	}

	private static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);
		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);
		return productModels;
	}
}
