package com.techu.apitechu3.controllers;

import com.techu.apitechu3.Apitechu3Application;
import com.techu.apitechu3.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.management.Descriptor;
import java.util.ArrayList;

@RestController
public class ProductController<getP> {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return Apitechu3Application.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductByid(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : Apitechu3Application.productModels){
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result =  product;
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        Apitechu3Application.productModels.add(newProduct);

        return newProduct;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")

        public ProductModel patchProduct(@RequestBody ProductModel productData,@PathVariable String id) {
            System.out.println("patchProduct2");
            System.out.println("La id del producto a actualizar en parámetro URL es " + id);
            System.out.println("La descripcion a actualizar es " + productData.getDesc());
            System.out.println("El precio del produto2 a actualizar es " + productData.getPrice());

            ProductModel result = new ProductModel();

            for(ProductModel productInList : Apitechu3Application.productModels){
                if(productInList.getId().equals(id)){
                    System.out.println("Producto encontrado");
                    result = productInList;

                if (productData.getDesc() != null){
                    System.out.println(("Actualizando descripcion del producto"));
                }
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() >0 ){
                    System.out.println("Actualizando precio del producto");
                    productInList.setPrice(productData.getPrice());
                }
            }

        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : Apitechu3Application.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;

            }
        }
        if(foundCompany == true) {
            System.out.println("Borrando producto");
            Apitechu3Application.productModels.remove(result);
        }

        return result;
    }
}
